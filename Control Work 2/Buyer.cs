﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_2
{
    public class Buyer
    {
        /// <summary>
        /// Дата рождения покупателя
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// Дата поломки обуви
        /// </summary>
        public DateTime Breakdown { get; set; }
    }
}
