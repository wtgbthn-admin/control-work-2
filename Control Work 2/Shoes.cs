﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_2
{
    public class Choes : Buyer
    {
        /// <summary>
        /// Дата создания обуви
        /// </summary>
        public DateTime Creation { get; set; }

        /// <summary>
        /// Дата покупки обуви
        /// </summary>
        public DateTime Purchase { get; set; }
    }
}